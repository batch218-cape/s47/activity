const txtFirstName = document.querySelector('#text-first-name');
const txtLastName = document.querySelector('#text-last-name');
const spanFullName = document.querySelector('#span-full-name');
function getFullname() {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
  }

txtFirstName.addEventListener('keyup', () => getFullname());

txtLastName.addEventListener('keyup', () => getFullname());
